NRG DICOM Image Utils
================================

The NRG DICOM Image Utils library provides tools for manipulating and managing
DICOM data.

> This library is a candidate to be combined with the [NRG DICOM Tools library]
(https://bitbucket.org/xnatdev/dicomtools


Building
--------

To build the NRG DICOM tools library, invoke Maven with the desired lifecycle phase.
For example, the following command cleans previous builds, builds a new jar file, 
creates archives containing the source code and JavaDocs for the library, runs the 
library's unit tests, and installs the jar into the local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mvn clean install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
